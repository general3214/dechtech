package dechmods.dechtech.gui;

import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.FurnaceRecipes;
import net.minecraft.tileentity.TileEntityFurnace;

public class SlotSmeltable extends Slot
{   
    public SlotSmeltable(IInventory tile, int slotID, int x, int y)
    {
        super(tile, slotID, x, y);
    }
    
    @Override
    public boolean isItemValid(ItemStack item)
    {
        return FurnaceRecipes.smelting().getSmeltingResult(item) != null;
    }
}
