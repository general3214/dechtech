package dechmods.dechtech.gui;

import java.util.ArrayList;
import java.util.List;

import org.lwjgl.opengl.GL11;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.Gui;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.inventory.Container;
import net.minecraft.util.ResourceLocation;

public class GUIGenerator extends GuiContainer
{
    ContainerGenerator generator;
    
    public GUIGenerator(ContainerGenerator generator)
    {
        super(generator);
        
        this.generator = generator;
        
        xSize = 176;
        ySize = 166;
    }
    
    @Override
    public void drawGuiContainerBackgroundLayer(float f, int i, int j)
    {
        GL11.glColor3f(1, 1, 1);
        
        Minecraft.getMinecraft().getTextureManager().bindTexture(new ResourceLocation("dechtech", "textures/gui/generator.png"));
        drawTexturedModalRect(guiLeft, guiTop, 0, 0, xSize, ySize);
        
        int height = (int)((generator.tile.chargeLevel / 160000F) * 64F);
        
        if (height > 0)
        {
            drawTexturedModalRect(guiLeft + 156, guiTop + 78 - height, 176, 14, 8, height);
            drawTexturedModalRect(guiLeft + 156, guiTop + 77, 176, 77, 8, 1);
        }
        
        int fuel = (int) Math.ceil(((float)generator.tile.currentFuelLevel / (float)generator.tile.lastMaxFuelLevel) * 13F);
        
        if (fuel > 0)
        {
            drawTexturedModalRect(guiLeft + 81, guiTop + 43 - fuel, 176, 13 - fuel, 14, fuel);
        }
    }
    
    @Override
    public void drawGuiContainerForegroundLayer(int x, int y) 
    {
        fontRenderer.drawString("Electric Generator", 8, 6, 0x404040);

        x -= guiLeft;
        y -= guiTop;
        
        if (x >= 156 && x < 164 && y >= 14 && y < 78)
        {
            List<String> text = new ArrayList<String>();
            text.add(generator.tile.chargeLevel + " MCEU");
            text.add("\u00a78Max 160000 MCEU");
            
            drawHoveringText(text, x, y, fontRenderer);
        }
    }
    
}
