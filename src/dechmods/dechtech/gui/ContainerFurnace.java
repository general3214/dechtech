package dechmods.dechtech.gui;

import dechmods.dechtech.tileentities.TileEntityElectricFurnace;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;

public class ContainerFurnace extends Container
{
    public TileEntityElectricFurnace tile;
    
    public ContainerFurnace(TileEntityElectricFurnace tileEntityElectricFurnace, InventoryPlayer invPlayer)
    {
        this.tile = tileEntityElectricFurnace;
        
        for (int x = 0; x < 9; x++)
        {
            addSlotToContainer(new Slot(invPlayer, x, 8 + 18 * x, 142));
        }
        
        for (int y = 0; y < 3; y++)
        {
            for (int x = 0; x < 9; x++)
            {
                addSlotToContainer(new Slot(invPlayer, x + y * 9 + 9, 8 + 18 * x, 84 + y * 18));
            }
        }
        
        addSlotToContainer(new SlotSmeltable(tileEntityElectricFurnace, 0, 44, 34));
        addSlotToContainer(new SlotOutput(tileEntityElectricFurnace, 1, 107, 34));
    }
    
    @Override
    public boolean canInteractWith(EntityPlayer entityplayer)
    {
        return tile.isUseableByPlayer(entityplayer);
    }   
    
    @Override
    public ItemStack transferStackInSlot(EntityPlayer player, int slotID)
    {
        return null;
    }
}
