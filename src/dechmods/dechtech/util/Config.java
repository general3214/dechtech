package dechmods.dechtech.util;

import net.minecraftforge.common.Configuration;

public class Config
{   
    public static int blockResourceID = 2350;
    public static int blockMachine1ID = 2351;
    public static int blockCableID = 2352;
    public static int itemIngotCopperID = 23500;
    
    public static void loadConfig(Configuration config)
    {
        config.load();
        
        blockResourceID = config.get("IDs", "BlockResourceID", 2350).getInt();
        blockMachine1ID = config.get("IDs", "BlockMachine1ID", 2351).getInt();
        blockCableID = config.get("IDs", "BlockCableID", 2352).getInt();
        itemIngotCopperID = config.get("IDs", "ItemIngotCopperID", 23500).getInt();
        
        config.save();
    }
}
