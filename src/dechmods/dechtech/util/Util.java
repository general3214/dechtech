package dechmods.dechtech.util;

import dechmods.dechtech.blocks.IElectricBlock;
import net.minecraft.block.Block;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;

public class Util
{   
    public static int basicPull(World world, int x, int y, int z, int amount)
    {
        TileEntity tile = world.getBlockTileEntity(x, y, z);
        
        if (world.isRemote || !(tile.blockType instanceof IElectricBlock)) return 0;
        
        IElectricBlock machine = (IElectricBlock) tile.blockType;
        int chargeLevel = machine.getCharge(world, x, y, z);
        
        if (chargeLevel > 0)
        {
            if (chargeLevel > amount)
            {
                machine.setCharge(world, x, y, z, chargeLevel - amount);
                return amount;
            }
            else
            {
                int diff = chargeLevel;
                machine.setCharge(world, x, y, z, 0);
                return diff;
            }
        }
        return 0;
    }
    
    public static int basicPush(World world, int x, int y, int z, int amount)
    {
        TileEntity tile = world.getBlockTileEntity(x, y, z);
        
        if (world.isRemote || !(tile.blockType instanceof IElectricBlock)) return 0;
        
        IElectricBlock machine = (IElectricBlock) tile.blockType;
        int chargeLevel = machine.getCharge(world, x, y, z);
        int maxCapacity = machine.getMaxCapacity(world, x, y, z);
        
        if (chargeLevel < maxCapacity)
        {
            if (chargeLevel + amount < maxCapacity)
            {
                machine.setCharge(world, x, y, z, chargeLevel + amount);
                return amount;
            }
            else
            {
                int diff = maxCapacity - chargeLevel;
                machine.setCharge(world, x, y, z, maxCapacity);
                return diff;
            }
        }
        return 0;
    }
    
    public static void saveInventory(NBTTagCompound compound, IInventory inv)
    {
        NBTTagList items = new NBTTagList();
        for (int i = 0; i < inv.getSizeInventory(); i++)
        {
            ItemStack stack = inv.getStackInSlot(i);
            
            if (stack != null)
            {
                NBTTagCompound item = new NBTTagCompound();
                item.setByte("Slot", (byte) i);
                stack.writeToNBT(item);
                items.appendTag(item);
            }
        }
        compound.setTag("Items", items);
    }
    
    public static void loadInventory(NBTTagCompound compound, IInventory inv)
    {
        NBTTagList items = compound.getTagList("Items");
        for (int i = 0; items.tagCount() > i; i++)
        {
            NBTTagCompound item = (NBTTagCompound) items.tagAt(i);
            int slot = item.getByte("Slot");
            
            if (slot >= 0 && slot < inv.getSizeInventory())
            {
                inv.setInventorySlotContents(slot, ItemStack.loadItemStackFromNBT(item));
            }
        }
    }

    public static boolean isCableBlock(IBlockAccess world, int x, int y, int z)
    {
        return world.getBlockId(x, y, z) == 501 || (world.getBlockId(x, y, z) != 0 && Block.blocksList[world.getBlockId(x, y, z)] instanceof IElectricBlock);
    }
}
