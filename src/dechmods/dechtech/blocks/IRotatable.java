package dechmods.dechtech.blocks;

import net.minecraft.world.World;

public interface IRotatable
{   
    public void rotateBlock(World world, int x, int y, int z, int side);
}
