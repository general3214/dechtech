package dechmods.dechtech.tileentities;

import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;

public abstract class TileEntityElectricBase extends TileEntity implements IElectricTileEntity
{   
    public int chargeLevel;
    
    @Override
    public void writeToNBT(NBTTagCompound compound)
    {
        super.writeToNBT(compound);
        compound.setInteger("chargeLevel", chargeLevel);
    }
    
    @Override
    public void readFromNBT(NBTTagCompound compound)
    {
        super.readFromNBT(compound);
        chargeLevel = compound.getInteger("chargeLevel");
    }
}
