package dechmods.dechtech;

import java.util.Arrays;


import net.minecraft.block.Block;
import net.minecraft.item.Item;
import net.minecraft.tileentity.TileEntity;
import net.minecraftforge.common.MinecraftForge;

import cpw.mods.fml.client.registry.RenderingRegistry;
import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.Instance;
import cpw.mods.fml.common.ModMetadata;
import cpw.mods.fml.common.Mod.EventHandler;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.event.FMLServerStartingEvent;
import cpw.mods.fml.common.network.NetworkMod;
import cpw.mods.fml.common.network.NetworkRegistry;
import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.relauncher.Side;
import dechmods.dechtech.blocks.BlockCable;
import dechmods.dechtech.blocks.BlockGenerator;
import dechmods.dechtech.blocks.BlockMachine;
import dechmods.dechtech.client.CableRenderer;
import dechmods.dechtech.gui.GUIHandler;
import dechmods.dechtech.gui.TooltipHandler;
import dechmods.dechtech.items.ItemBlockMachine;
import dechmods.dechtech.network.PacketHandler;
import dechmods.dechtech.tileentities.TileEntityElectricFurnace;
import dechmods.dechtech.tileentities.TileEntityGenerator;
import dechmods.dechtech.util.Config;

@Mod(modid = "dechtech", name = "DeCh-Tech", version = "0.0.1")
@NetworkMod(clientSideRequired = true, serverSideRequired = false)
public class DeChTech
{
    public static Block blockMachine1, blockCable;
    
    @Instance("dechtech")
    public static DeChTech instance;
    
    @EventHandler
    public void preLoad(FMLPreInitializationEvent event)
    {
        MinecraftForge.EVENT_BUS.register(new TooltipHandler());
    }
    
    @EventHandler
    public void load(FMLInitializationEvent event)
    {
        TileEntity.addMapping(TileEntityGenerator.class, "dechtech_generator");
        TileEntity.addMapping(TileEntityElectricFurnace.class, "dechtech_furnace");
        NetworkRegistry.instance().registerGuiHandler(this, new GUIHandler());
        
        NetworkRegistry.instance().registerChannel(new PacketHandler(), "packetdechtech");
        
        blockMachine1 = new BlockMachine(Config.blockMachine1ID).setHardness(2.5F).setResistance(2F).setUnlocalizedName("blockMachine1");
        Item.itemsList[Config.blockMachine1ID] = new ItemBlockMachine(Config.blockMachine1ID).setUnlocalizedName("blockMachine1");
        
        Block block = new BlockGenerator(500).setHardness(2.5F).setResistance(2F).setUnlocalizedName("blockGenerator");
        GameRegistry.registerBlock(block, "blockGenerator");
        
        blockCable = new BlockCable(501).setHardness(2.5F).setResistance(2F).setUnlocalizedName("blockCable");
        GameRegistry.registerBlock(blockCable, "blockCable");
        
        if (FMLCommonHandler.instance().getSide() == Side.CLIENT) RenderingRegistry.registerBlockHandler(new CableRenderer(RenderingRegistry.getNextAvailableRenderId()));
    }
}